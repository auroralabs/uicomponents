# UI Components #

## Components list ##
* [SearchView](https://bitbucket.org/auroralabs/uicomponents/src/master/SEARCHVIEW.md)

## Installation ##

Add this entries to build.gradle:

```java
   repositories {
       ...
       maven { url "https://jitpack.io" }
   }
```

```java

    implementation "org.bitbucket.auroralabs:uicomponents:v1.0.2"
```
  
