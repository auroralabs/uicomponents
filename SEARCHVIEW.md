# SearchView #

![Image of SearchView](ui-components/images/searchview.png)

Implements a Material's alike search view.

TextView theme is configurable trough android:theme

## Style ##

The following styles are available:

* roundRadio: set component round radio.
* scrimsTint: set search and clean scrims color
* backgroundColor: set component's background color
* placeholder: set EditText hint.

## Use ##

Add es.auroralabs.uicomponents.SearchView to layout:

```
        <es.auroralabs.uicomponents.SearchView
            android:id="@+id/searchView"
            android:theme="@style/Theme.AppCompat.Light"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_margin="4dp"
            app:roundRadio="4dp"
            app:scrimsTint="@color/colorPrimary"
            />
```

If needed, and should be needed, set SearchView search callback:

```
findViewById<SearchView>(R.id.searchView)?.let {
                it.setOnSearchCallback { text, isComplete ->
                    if (isComplete) {
                        Snackbar.make(it, text, Snackbar.LENGTH_LONG).show()
                    }
                }
        }
```

* text: Is the text filled by the user.
* isComplete: true if the user press search button, false otherwise.
