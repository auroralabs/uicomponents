package es.auroralabs.material

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import es.auroralabs.uicomponents.SearchView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        findViewById<SearchView>(R.id.searchView)?.let {
                it.setOnSearchCallback { text, isComplete ->
                    if (isComplete) {
                        Snackbar.make(it, text, Snackbar.LENGTH_LONG).show()
                    }
                }
        }
    }
}
