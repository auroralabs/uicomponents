package es.auroralabs.uicomponents

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.ImageViewCompat
import es.auroralabs.materialui.R

/**
 * Material's Search Bar implementation.
 * search callback: (textSearched:String, searchComplete:Boolean) -> Unit
 */
class SearchView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val defaultColor = Color.parseColor("#8a000000")
    private var scrimsColor = defaultColor
    private var searchBackgroundColor = Color.WHITE
    private var roundRadio = 0
    private var placeholder = ""

    private val searchEditText: EditText by lazy { findViewById<EditText>(R.id.searchEditText) }
    private val clearView: ImageView by lazy { findViewById<ImageView>(R.id.clearIcon) }
    private val searchIcon: ImageView by lazy { findViewById<ImageView>(R.id.searchIcon) }
    private var callback: ((textSearched: String, searchComplete: Boolean) -> Unit)? = null
    private val viewOutlineProvider: ViewOutlineProvider = object : ViewOutlineProvider() {
        override fun getOutline(view: View, outline: Outline) {
            outline.setRoundRect(0, 0, view.width, view.height, roundRadio.toFloat())
        }
    }
    private val internalTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            // Nothing
        }

        override fun beforeTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (text?.length ?: 0 > 0) {
                clearView.visibility = View.VISIBLE
            } else {
                clearView.visibility = View.INVISIBLE
            }
        }

        override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
            text?.let {
                callback?.invoke(it.toString(), false)
            }
        }
    }
    init {
        LayoutInflater.from(context).inflate(R.layout.search_view, this, true)
        setCustomAtributes(attrs, context)
        clearView.visibility = View.INVISIBLE
        outlineProvider = viewOutlineProvider
        clipToOutline = true
        configureSearchEditText()
        applyAttributes()
    }

    private fun configureSearchEditText() {
        searchEditText.addTextChangedListener(internalTextWatcher)
        searchEditText.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchEditText.clearComposingText()
                searchEditText.clearFocus()
                hideKeyboard(textView)
                callback?.invoke(textView.text.toString(), true)
                true
            } else {
                false
            }
        }
        clearView.setOnClickListener {
            searchEditText.clearComposingText()
            searchEditText.clearFocus()
            searchEditText.text.clear()
            clearView.visibility = View.INVISIBLE
        }
    }

    private fun hideKeyboard(textView: TextView) {
        val imm: InputMethodManager? =
            textView.context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(textView.windowToken, 0)
    }

    private fun applyAttributes() {
        ImageViewCompat.setImageTintList(clearView, ColorStateList.valueOf(scrimsColor))
        ImageViewCompat.setImageTintList(searchIcon, ColorStateList.valueOf(scrimsColor))
        searchEditText.hint = placeholder
        backgroundTintList = ColorStateList.valueOf(searchBackgroundColor)
        background = ColorDrawable(searchBackgroundColor)
    }

    private fun setCustomAtributes(attrs: AttributeSet?, context: Context) {
        attrs?.let { attributeSet ->
            val attributeArray: TypedArray = context.theme.obtainStyledAttributes(
                attributeSet,
                R.styleable.SearchView, 0, 0
            )
            scrimsColor = attributeArray.getColor(
                R.styleable.SearchView_scrimsTint, defaultColor
            )
            searchBackgroundColor = attributeArray.getColor(
                R.styleable.SearchView_backgroundColor, Color.WHITE
            )
            roundRadio = attributeArray.getDimensionPixelSize(
                R.styleable.SearchView_roundRadio, 0
            )
            placeholder = attributeArray.getString(R.styleable.SearchView_placeholder) ?: ""
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val fixedHeight = resources.getDimensionPixelSize(R.dimen.search_view_height)

        setMeasuredDimension(widthSize, fixedHeight)

    }

    fun setOnSearchCallback(withCallback: ((String, Boolean) -> Unit)?) {
        callback = withCallback
    }

    fun setPlaceholder(placeholder: String) {
        searchEditText.hint = placeholder
    }

    fun getSearchQuery() = searchEditText.text.toString()

    fun clear() = searchEditText.text.clear()
}
